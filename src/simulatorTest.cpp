#include <iostream>
#include "simulator.h"
#include "panel.h"
#include "inverter.h"
#include "wire.h"

int main() {


    Panel *panel1 = new Panel(4850, 10, 5, 40, 10, 1.5, 5.5,"5 5 5 N");
    
    Wire *wire1 = new Wire(2, 30);
    
    Inverter *inverter1 = new Inverter(97);

    Simulator sim(panel1, wire1, inverter1);

    float *yearlyOutputs;
    yearlyOutputs = sim.simulate(25);

    int i = 0;
    for (i = 0; i < 25; i++) {
        std::cout << i << ": " << yearlyOutputs[i] << std::endl;
    }

/*    panel1->~Panel();
    wire1->~Wire();
    inverter1->~Inverter();
*/
    return 0;
}

//4850	# rated number of watts output by bank of panels
//10		# panels are installed pointing 10 degrees west of north
//5	  	# panels' output is reduced by 5% due to orientation
//40		# panels are installed at 40 degrees from horizontal
//10		# panels' output is reduced by 10% due to installation angle
//30		# wiring from panels to inverter is 30 metres in length
//2	  	# this length of wiring causes a power loss of 2%
//97		# the inverter is on average 97% efficient when close to rated load
//1.5		# the panels' output will reduce by 1.5% per year
//5.5		# the average number of hours of effective sunshine is 5.5 at this location
