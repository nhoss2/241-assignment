
#include <iostream>

using std::cout;
using std::endl;

#include "wire.h"

int main() {


    cout << "creating a Wire object with efficiency loss of 2\% and length of 5" << endl;
    Wire *wire1 = new Wire(2,5);

    cout << "Wire Output from 100W input " << wire1->getOutputPower(100) << "W" << endl;

    cout << "Wire efficiency loss: " << wire1->getEfficiency() << endl;
    cout << "Wire length: " << wire1->getWireLength() << endl;

    //Clean up and deallocate memory.
    wire1->~Wire();

    return 0;

}

