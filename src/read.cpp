// reading a text file
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstdlib>
using namespace std;

#include "read.h"

const int dataSetSize = 10;

Read::Read() {};

Read::Read(char *inputFile){
	
  string buffer[dataSetSize];
  int count = 0;
  ifstream myfile (inputFile);
  if (myfile.is_open())
  {
    while ( myfile.good() )
    {
      getline (myfile, buffer[count]);
	  count++;
    }
    myfile.close();
  }

  else cout << "Unable to open file \n"; 

/*  if (!ratedWatts)
	  ratedWatts = new float;

  if (!degreesWest)
	  degreesWest = new float;

 if (!orientationLoss)
	  orientationLoss = new float;

 if (!degreesHorizontal)
	  degreesHorizontal = new float;

 if (!angleLoss)
	  angleLoss = new float;

 if (!wireLength)
	 wireLength = new float;

 if (!wireLoss)
	  wireLoss = new float;

 if (!inverterEfficiency)
	  inverterEfficiency = new float;

 if (!reductionPerYear)
	  reductionPerYear = new float;

 if (!effectiveSunshineHrs)
	 effectiveSunshineHrs = new float;
*/

  ratedWatts = atof(buffer[0].c_str());
  degreesWest = atof(buffer[1].c_str());
  orientationLoss = atof(buffer[2].c_str());
  degreesHorizontal = atof(buffer[3].c_str());
  angleLoss = atof(buffer[4].c_str());
  wireLength = atof(buffer[5].c_str());
  wireLoss = atof(buffer[6].c_str());
  inverterEfficiency = atof(buffer[7].c_str());
  reductionPerYear = atof(buffer[8].c_str());
  effectiveSunshineHrs = atof(buffer[9].c_str());

}

Read::~Read() {}

string Read::stringRepresentation() const
{
   // Create string to display some Read data members. 
   stringstream numberString;
   numberString << ratedWatts << '\n' << degreesWest << '\n' << orientationLoss << '\n' << degreesHorizontal << '\n' << angleLoss << '\n' << wireLength << '\n' << wireLoss << '\n' << inverterEfficiency << '\n' << reductionPerYear << '\n' << effectiveSunshineHrs << endl;

   return numberString.str();
}
