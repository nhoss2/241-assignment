/*
 *
 *
 *
 *
 */

#include <iostream>
using std::cout;
using std::endl;

#include "read.h"
#include "panel.h"
#include "wire.h"
#include "inverter.h"
#include "simulator.h"
#include "output.h"

int main(){

	Read reading1("dataset.txt");

	float newRatedWatts;
	float newDegreesWest;
	float newOrientationLoss;
	float newDegreesHorizontal;
	float newAngleLoss;
	float newWireLength;
	float newWireLoss;
	float newInverterEfficiency;
	float newReductionPerYear;
	float newEffectiveSunshineHrs;

	newRatedWatts = reading1.getRatedWatts();
	newDegreesWest = reading1.getDegreesWest();
	newOrientationLoss = reading1.getOrientationLoss();
	newDegreesHorizontal = reading1.getDegreesHorizontal();
	newAngleLoss = reading1.getAngleLoss();
	newWireLength = reading1.getWireLength();
	newWireLoss = reading1.getWireLoss();
	newInverterEfficiency = reading1.getInverterEfficiency();
	newReductionPerYear = reading1.getReductionPerYear();
	newEffectiveSunshineHrs = reading1.getEffectiveSunshineHrs();

	// reading1->~Read();

	Panel *panel1 = new Panel(newRatedWatts, newDegreesWest, newOrientationLoss, newDegreesHorizontal, newAngleLoss, newReductionPerYear, newEffectiveSunshineHrs,"5 5 5 N");


	Wire *wire1 = new Wire(newWireLoss, newWireLength);

	Inverter *inverter1 = new Inverter(newInverterEfficiency);

	std::cout << "RatedWatts: " << newRatedWatts << "\t" << "\t" << "Original Rated Watts: 4850" << std::endl;
	std::cout << "DegreesWest: " << newDegreesWest << "\t" << "\t"<< "\t" << "Original Degrees West: 10" << std::endl;
	std::cout << "OrientationLoss: " << newOrientationLoss << "\t" << "\t" << "Original OrientationLoss: 5" << std::endl;
	std::cout << "DegreesHorizontal: " << newDegreesHorizontal << "\t" << "\t" << "Original DegreesHorizontal: 40" << std::endl;
	std::cout << "AngleLoss: " << newAngleLoss << "\t" << "\t" << "\t" << "Original AngleLoss: 10" << std::endl;
	std::cout << "WireLength: " << newWireLength << "\t" << "\t" << "\t" << "Original WireLength: 30" << std::endl;
	std::cout << "WireLoss: " << newWireLoss << "\t" << "\t" << "\t" << "Original WireLoss: 2" << std::endl;
	std::cout << "InverterEfficiency: " << newInverterEfficiency << "\t" << "\t" << "Original InverterEfficiency: 97" << std::endl;
	std::cout << "ReductionPerYear: " << newReductionPerYear << "\t" << "\t" << "Original ReductionPerYear: 1.5" << std::endl;
	std::cout << "EffectiveSunshineHrs: " << newEffectiveSunshineHrs << "\t" << "Original EffectiveSunshineHrs: 5.5" << std::endl << std::endl; 
	
	Simulator sim(panel1, wire1, inverter1);

	float *yearlyOutputs;
	yearlyOutputs = sim.simulate(25);

	int i = 0;
	for (i = 0; i < 25; i++) {
		std::cout << i << ": " << yearlyOutputs[i] << std::endl;
	}

	return 0;
}
//4850
//10
//5
//40
//10
//30
//2
//97
//1.5
//5.5
