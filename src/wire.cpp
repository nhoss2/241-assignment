#include <cmath>
#include "wire.h"
#include "part.h"
#include <iostream>

Wire::Wire(const float wireEfficiency, const float wireLength)
    : Part(wireEfficiency) {

      std::cout << "CREATING NEW WIRE" << std::endl;

    if (!_wireLength) {
        std::cout << "CREATING NEW WIRE" << std::endl;
        _wireLength = new float;
        std::cout << "CREATING NEW WIRE" << std::endl;
    }

    if (validateInputs(wireLength)) {
        std::cout << "WRITING WIRE LENGTH" << std::endl;
        *_wireLength = wireLength;
        std::cout << "WRITING WIRE LENGTH: " << std::endl;
    } else {
        // return exception
    }
}

//Default Deconstructor used to correctly cleanup the memory
Wire::~Wire() {
    delete _wireLength;
    *_wireLength = 0;
}

/*
 * validateInputs - used to validate inputs in the constructor.
 * We only need to validate the wire length and the wire efficiency
 * is validated by the base class (part.h) constructor.
 */
bool Wire::validateInputs(const float wireLength) {

    if (wireLength > 0) return true;
    return false;
}

// accessor for wire length
float Wire::getWireLength() const {
    return *_wireLength;
}
