/*  outputTest.cpp
 *
 *  test driver for the output class
 *
 *  author: manoli stamatiou
 *
 *  date: 25/04/2013
 *
 */

#include <iostream>
#include "output.h"

int main() {

    float powerPerYear[24] = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400};
    float avgPowerPerDay[24] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240};

    //tests the member function that prints to console
    Output* test = new Output(powerPerYear, avgPowerPerDay);
    test->printOutput();

    //tests the member function that prints to file
    Output* test2 = new Output(powerPerYear, avgPowerPerDay);
    test2->toFile();

    test->~Output();
    test2->~Output();

    return 0;
}
