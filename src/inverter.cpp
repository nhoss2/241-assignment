#include "inverter.h"

Inverter::Inverter(const float inverterEfficiency) : Part(100 - inverterEfficiency) {
    // empty constructor as the inverterEfficiency gets validated
    // and saved in the constructor of part class
}

Inverter::~Inverter() {
    // empty deconstructor for now as _efficiency gets deleted
    // in the base class (part.h) deconstructor.
}
