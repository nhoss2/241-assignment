#include <cmath>
#include <string>
#include <sstream>
#include <cstdlib>
#include "panel.h"
#include <iostream>

using namespace std;

// Default constructor. Initialises the object to some default values (0)
Panel::Panel() {

    set_panel();
}
//Default Deconstructor used to correctly cleanup the memory by dea
Panel::~Panel() {

}

//Constructor used to input data, calls the set_panel function
Panel::Panel(const float ratedWatts, const float degreesWest, const float orientationLoss, const float degreesHorizontal, const float angleLoss, const float reductionPerYear, const float effectiveSunshineHrs, std::string latitude) {



    set_panel(ratedWatts, degreesWest, orientationLoss, degreesHorizontal, angleLoss, reductionPerYear, effectiveSunshineHrs, latitude);
}


//Set_panel constructor allocates memory and sets all private member variables to input values,
void Panel::set_panel(const float ratedWatts, const float degreesWest, const float orientationLoss, const float degreesHorizontal, const float angleLoss, const float reductionPerYear, const float effectiveSunshineHrs, std::string latitude) {
	
    panelData[0] = ratedWatts;
    panelData[2] = orientationLoss/100;
    panelData[4] = angleLoss/100;
    panelData[5] = reductionPerYear;
    panelData[6] = effectiveSunshineHrs;

    _valid = is_valid_panelinput();

    panelData[3] = angle(degreesHorizontal);
    panelData[1] = angle(degreesWest);
	panelData[7] = latitudeToAngle(latitude);
    panelData[8] = panelOutput();
	

}
//This function multiples the rated power of the panels by the efficiences due to non optimal angles and stores the resulting value.
float Panel::panelOutput() {

    return (getRatedWatts()*(1-getOrientationLoss())*(1-getAngleLoss()));


}
//Sanity checks for the input data. If input is not sane then we assume all inputs are garbage and thus set all private member variables to false. NOTE: This will eventually throw an exception  to be catched instead.
bool Panel::is_valid_panelinput() {

    if (( getOrientationLoss()||getAngleLoss()||getReductionPerYear()||getEffectiveSunshineHrs()) < 0 || (getOrientationLoss()||getAngleLoss()||getReductionPerYear()) > 1)
        return false;

    if ((getEffectiveSunshineHrs()) < 0 || (getEffectiveSunshineHrs() > 24))
        return false;

    if (getRatedWatts()==0)
        return false;

    return true;

}

//Normalises degrees and converts to radians.
float Panel::angle(const float angle) {

    float _angle = angle;
    //While angle is not normalised to 360 degrees.
    while(_angle > 360 || _angle < 0) {

        _angle = fmod(_angle,360); // fmod is used to find the modulas of a float.

    }

    return _angle;//returns the angle in radians uses a semi accurate floating point representation of pi.
}


// Converts the latitude Deg:Min:Sec format to a decimal place
float Panel::latitudeToAngle(const string latitude) {

	stringstream conv;
	string angleData[4];
	
	conv<<latitude; // Puts "latitude into stream for manipulation"
	
	for (int i=0; i<4; i++) 
		getline(conv, angleData[i], ' '); // Extracts information from stream using a space as the delimiter

	// Converts the Min and Sec to a fraction of degrees and adds
	float optAngle = atof(angleData[0].c_str()) + atof(angleData[1].c_str())/60 + atof(angleData[2].c_str())/3600;
	
	// Checks if latitude is in south or north hemisphere and converts angle to - if in the south
	if ((angleData[3]=="S") || (angleData[3]=="s"))
		optAngle = optAngle*-1;

	return optAngle;

}

// Takes the latitude angle and produces a string to show the angle and which way the solar panel is to tilt
string Panel::optAngleRep(const float angle) {

	stringstream optAngle; 

	if (angle<0)									// If angle is negative, position is in southern hemisphere
		optAngle << (angle*-1) << " degrees tilted north";	// and the solar panel should tilt north 
	else 
		optAngle << angle << " degrees tilted south";		// Similarly, if in the north, Panel should tilt south

	return optAngle.str();
	
}


