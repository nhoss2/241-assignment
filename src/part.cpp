#include "part.h"
#include <iostream>

using namespace std;

// Constructor
Part::Part(const float efficiency) {

    if (!_efficiency) {
        _efficiency = new float;
    }

    // if efficiency is valid, set the efficiency
    if (validateInputs(efficiency)) {
       * _efficiency = efficiency/100;
    } else {
      // exception here
      // temporary cout, remember to remove iostream when you remove cout
      std::cout << "error: invalid efficiency" << std::endl;
    }

}

// Deconstructor
Part::~Part() {
    delete _efficiency;
    *_efficiency=0;
}

/*
 * validateInputs - Returns a boolean after validating
 *     if the given efficiency is correct.
 *
 * This is a virtual member function and it should be overwritten
 * by derived classes, to add validation to their own constructor
 * inputs
 */
bool Part::validateInputs(const float efficiency) {

    if (efficiency >= 0 || efficiency <= 100) return true;
    else return false;

}

// Accessor for _efficiency
float Part::getEfficiency() const {
    return *_efficiency;
}

/*
 * getOutputpower - Returns the power output the current
 *    'part' would output after the efficiency loss.
 *
 * This is another virtual memeber function.
 */
float Part::getOutputPower(const float inputPower) const {
    return inputPower * (1 - *_efficiency);
}
