/*  output.cpp
 *
 *  implementation of the output constructors and member functions
 *
 *  author: manoli stamatiou
 *
 *  date: 25/04/2013
 *
 */

#include "output.h"
#include <iostream>
using std::cout;
#include <fstream>
using std::ofstream;
#include <string>
using std::string;
#include <sstream>
using std::stringstream;

//default constructor
Output::Output() {

}

//default deconstructor used to directly clean up memory from dynamically allocated memory
Output::~Output() {

    delete [] _powerPerYear;
    delete [] _avgPowerPerDay;

    _powerPerYear = 0;
    _avgPowerPerDay = 0;
    cout << "Dynamically Allocated Memory has been deleted" << '\n' << '\n';
}

//constructor with input parameters
//calls setOutput() member function
Output::Output(const float powerPerYear[24], const float avgPowerPerDay[24]) {

    setOutput(powerPerYear, avgPowerPerDay);
}

//void type member function which allocates memory and sets all private member variables to input values
void Output::setOutput(const float powerPerYear[24], const float avgPowerPerDay[24]) {

    if(!_powerPerYear) {
        _powerPerYear = new float[24];
    }
    if(!_avgPowerPerDay) {
        _avgPowerPerDay = new float[24];
    }

    //for loop to set every element of the private member arrays to its respective input arrays
    for(int row = 0; row < 24; row++) {
        _powerPerYear[row] = powerPerYear[row];
        _avgPowerPerDay[row] = avgPowerPerDay[row];
    }
}

//string type member function which converts stream into a single string ready for output
//returns a string
string Output::toString() {
    //formatting for the header of the output table
    stringstream tableHeader;
    tableHeader << "=========================================================" << '\n'
                << "| Years" << '\t' << "| " << "Total KW for the Year" << '\t' << "| " << "Average Kw/Day" << '\t' << "|" << '\n'
                << "=========================================================" << '\n';

    //formatting for the output table values
    stringstream tableValues;
    for(int row = 0; row < 24; row++) {
        tableValues << "| " << row+1 << '\t' << "| " << _powerPerYear[row] << " KW" << '\t' << '\t' << "| " << _avgPowerPerDay[row] << " KW/Day" << '\t' << '\t' << "|" << '\n'
                    << "---------------------------------------------------------" << '\n';
    }

    //converting the streams to string and adding the two strings to form one string
    stringstream outputTable;
    outputTable << tableHeader.str() + tableValues.str();

    return outputTable.str();
}

//void type member function which outputs to terminal
void Output::printOutput() {
    //formatting the output table
    cout << "=========================================================" << '\n';
    cout << "| Years" << '\t' << "| " << "Total KW for the Year" << '\t' << "| " << "Average Kw/Day" << '\t' << "|" << '\n';
    cout << "=========================================================" << '\n';
    for(int row = 0; row < 24; row++) {
        cout << "| " << row+1 << '\t' << "| " << _powerPerYear[row] << " KW" << '\t' << '\t' << "| " << _avgPowerPerDay[row] << " KW/Day" << '\t' << '\t' << "|" << '\n';
        cout << "---------------------------------------------------------" << '\n';
    }
//    cout << toString() << '\n';
}

//void type member function which creates a file and writes the toString() output to the file
void Output::toFile() {
    string toText = toString();
    ofstream myfile;
    myfile.open("Solar_Panel_Performance.txt");
    myfile << toText;
    myfile.close();
    cout << '\n' << "Generated Table to Solar_Panel_Performance.txt" << '\n' << '\n';
}
