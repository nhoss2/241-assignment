#include "simulator.h"
#include <iostream>

Simulator::Simulator(Panel *panel, Wire *wire, Inverter *inverter ) {

    // save the parts as member variables
    _panel = panel;
    std::cout << "_panel: " << _panel << std::endl;

    _wire = wire;
    std::cout << "_wire: " << _wire << std::endl;
    
    _inverter = inverter;
    std::cout << "_inverter: " << _inverter << std::endl;

    // get the panel loss per year and average sunshine from the panel object
    _avgSunshineDay = panel->getEffectiveSunshineHrs();
    std::cout << "_avgSunshineDay: " << _avgSunshineDay << std::endl;

    _panelLossYear = panel->getReductionPerYear() / 100;
    std::cout << "_panelLossYear: " << _panelLossYear << std::endl;
};

// empty deconstructor
Simulator::~Simulator() { };

/*
 * Function simulate:
 * Gets the power outputs after losses from each part of the system.
 * Returns a pointer to a float array with yearly power outputs
 *
 * inputs:
 *   int years - number of years to simulate
 */
float * Simulator::simulate(const int years) {

    // initialise variable for loop
    int currentYear = 0;
    std::cout << "Current year: " << currentYear << std::endl;

    // initialise output array
    float* yearlyOutputs;
    yearlyOutputs = new float[years - 1]; // stores power output of solar system for each year

    // a for loop 'simulates' running the solar panels through one year
    for (currentYear = 0; currentYear < years; currentYear++) {
        float panelPower = _panel->getOutputPower(); // output power from panel after loss due to orientation, installation angle
	std::cout << "PanelPower: " << panelPower << std::endl;

        // simulate degradation of panels
        if (currentYear > 0) {
            panelPower = panelPower * ( 1 - _panelLossYear * currentYear );
	    std::cout << "panelPower After Degredation: " << panelPower << std::endl;
        }

        float powerAfterWire = _wire->getOutputPower(panelPower); // panel power after wire power loss
	std::cout << "powerAfterWire: " << powerAfterWire << std::endl;

        float powerAfterInverter = _inverter->getOutputPower(powerAfterWire); // power after panel, wire and inverter loss
	std::cout << "powerAfterInverter: " << powerAfterInverter << std::endl;

        // output
        float powerDay = powerAfterInverter * _avgSunshineDay;
	std::cout << "powerDay: " << powerDay << std::endl;

        float powerYear = powerDay * 365;
	std::cout << "powerYear: " << powerYear << std::endl;

        yearlyOutputs[currentYear] = powerYear;

        std::cout << "sim: " << powerYear << std::endl;
    }

    return yearlyOutputs;

}

//4850	# rated number of watts output by bank of panels
//10		# panels are installed pointing 10 degrees west of north
//5	  	# panels' output is reduced by 5% due to orientation
//40		# panels are installed at 40 degrees from horizontal
//10		# panels' output is reduced by 10% due to installation angle
//30		# wiring from panels to inverter is 30 metres in length
//2	  	# this length of wiring causes a power loss of 2%
//97		# the inverter is on average 97% efficient when close to rated load
//1.5		# the panels' output will reduce by 1.5% per year
//5.5		# the average number of hours of effective sunshine is 5.5 at this location
