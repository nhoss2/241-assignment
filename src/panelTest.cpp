
#include <iostream>

	using std::cout;
	using std::endl;

#include "panel.h"





 int main(){

	
 Panel p(8000,5,10,40,10,1.5,29,"5 5 5 N");
   
   	cout << "Solar Panel Test Driver Output" << endl;
	cout << "=====================================" << endl;
	//Output all stored private member variables from the panel class to console.

		
		cout << "Output for Solar panel " << 1 << endl;
		cout << "Panel Power: " << p.getRatedWatts() << endl;
		cout << "Panel Orientation Loss: " << p.getOrientationLoss() << endl;
		cout << "Panel Orientation Angle: " << p.getDegreesWest()  << endl;
		cout << "Panel Install Angle Loss: " << p.getAngleLoss() << endl;
		cout << "Panel Install Angle: " << p. getDegreesHorizontal() << endl;
		cout << "Panel Efficiency Loss PY: " << p.getReductionPerYear() << endl;
		cout << "Hours of Sunlight PD: " << p.getEffectiveSunshineHrs()  << endl;
		cout << "Panel Optimum Angle: " << p.getOpAngle() << endl;
		cout << "Panel Optimum Angle: " << p.optAngleRep(p.getOpAngle()) << endl;
		cout << "Panel Output Power: " << p.getOutputPower() << endl;

		cout << endl << endl << endl;
      
   
   //Clean up and deallocate memory. 

    return 0;
  
}

