#include <iostream>
#include "inverter.h"

using namespace std;

int main() {

    Inverter *inverter1 = new Inverter(2);

    cout << "Inverter output from 100W input" << inverter1->getOutputPower(100) << endl;

    cout << "Inverter Efficiency: " << inverter1->getEfficiency() << endl;

    //Clean up and deallocate memory.
    inverter1->~Inverter();

    return 0;
}
