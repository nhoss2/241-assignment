all : obj/part.o inverterTest outputTest panelTest readTest wireTest simulatorTest highTest

################################## part  #########################################
obj/part.o : include/part.h src/part.cpp
	g++ -ansi -c -o obj/part.o -I include/ src/part.cpp

################################## inverter #########################################
obj/inverter.o : include/part.h include/inverter.h src/inverter.cpp
	g++ -ansi -c -o obj/inverter.o -I include/ src/inverter.cpp

obj/inverterTest.o : include/part.h include/inverter.h src/inverterTest.cpp
	g++ -ansi -c -o obj/inverterTest.o -I include/ src/inverterTest.cpp

inverterTest : obj/inverter.o obj/inverterTest.o
	g++ -o bin/inverterTest obj/inverter.o obj/inverterTest.o obj/part.o

################################## output #########################################
obj/output.o : include/output.h src/output.cpp
	g++ -ansi -c -o obj/output.o -I include/ src/output.cpp

obj/outputTest.o : include/output.h src/outputTest.cpp
	g++ -ansi -c -o obj/outputTest.o -I include/ src/outputTest.cpp

outputTest : obj/output.o obj/outputTest.o
	g++ -o bin/outputTest obj/output.o obj/outputTest.o

################################## panel  #########################################
obj/panel.o : include/panel.h src/panel.cpp
	g++ -ansi -c -o obj/panel.o -I include/ src/panel.cpp

obj/panelTest.o : include/panel.h src/panelTest.cpp
	g++ -ansi -c -o obj/panelTest.o -I include/ src/panelTest.cpp

panelTest : obj/panel.o obj/panelTest.o
	g++ -o bin/panelTest obj/panel.o obj/panelTest.o

################################## read  #########################################
obj/read.o : include/read.h src/read.cpp
	g++ -ansi -c -o obj/read.o -I include/ src/read.cpp

obj/readTest.o : include/read.h src/readTest.cpp
	g++ -ansi -c -o obj/readTest.o -I include/ src/readTest.cpp

readTest : obj/read.o obj/readTest.o
	g++ -o bin/readTest obj/read.o obj/readTest.o

################################## wire  #########################################
obj/wire.o : include/part.h include/wire.h src/wire.cpp
	g++ -ansi -c -o obj/wire.o -I include/ src/wire.cpp

obj/wireTest.o : include/part.h include/wire.h src/wireTest.cpp
	g++ -ansi -c -o obj/wireTest.o -I include/ src/wireTest.cpp

wireTest : obj/wire.o obj/wireTest.o
	g++ -o bin/wireTest obj/wire.o obj/wireTest.o obj/part.o

################################## simulator  #########################################
obj/simulator.o : include/simulator.h src/simulator.cpp
	g++ -ansi -c -o obj/simulator.o -I include/ src/simulator.cpp

obj/simulatorTest.o : include/simulator.h src/simulatorTest.cpp
	g++ -ansi -c -o obj/simulatorTest.o -I include/ src/simulatorTest.cpp

simulatorTest : obj/simulator.o obj/simulatorTest.o obj/wire.o obj/panel.o obj/inverter.o obj/part.o
	g++ -o bin/simulatorTest obj/simulator.o obj/simulatorTest.o obj/wire.o obj/panel.o obj/inverter.o obj/part.o

################################## hightest  #########################################
obj/highTest.o : src/highTest.cpp
	g++ -ansi -c -o obj/highTest.o -I include/ src/highTest.cpp

highTest : obj/highTest.o
	g++ -o bin/highTest obj/read.o obj/highTest.o obj/wire.o obj/panel.o obj/inverter.o obj/simulator.o obj/output.o obj/part.o
