#ifndef PART_H
#define PART_H

/*
 * TODO: COMMMEENNTSSS
 */

class Part {

protected:
    float *_efficiency;

    virtual bool validateInputs(const float efficiency);

public:
    Part(const float efficiency);

    ~Part();

    float getEfficiency() const;

    virtual float getOutputPower(const float inputPower) const;
};

#endif
