#include "panel.h"
#include "wire.h"
#include "inverter.h"

class Simulator {

private:

    // In the future, will probably extend this to handle multiple objects
    Panel *_panel;
    Wire *_wire;
    Inverter *_inverter;

    float _panelLossYear;
    float _avgSunshineDay;

public:

    Simulator(Panel *panel, Wire *wire, Inverter *inverter); // default constructor

    ~Simulator(); // deconstructor

    float *simulate(int years); // simulator

};
