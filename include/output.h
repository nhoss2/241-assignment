/*  output.h
 *
 *  implementation of the output class
 *
 *  author: manoli stamatiou
 *
 *  date: 25/04/2013
 *
 */

//define include guards to protect against multiple inclusions and thus potential conflitcts
#ifndef _OUTPUT_H
#define _OUTPUT_H

#include <string>
using std::string;

class Output {

public:
    Output();//default constructor
    ~Output();//deconstructor
    Output(const float powerPerYear[24], const float avgPowerPerDay[24]);//constructor with input parameters

    void setOutput(const float powerPerYear[24], const float avgPowerPerDay[24]);//member function prototype to set the member variables

    void printOutput();//member function prototype to print the output to console

    void toFile();//member function prototype which writes to text file

    string toString();//member function prototype that converts stream into string

    //accessor
    float get_powerPerYear() const {
        return _powerPerYear[24];
    }

    //accessor
    float get_avgPowerPerDay() const {
        return _avgPowerPerDay[24];
    }

    //private member variables
private:
    float *_powerPerYear;
    float *_avgPowerPerDay;

};

#endif

