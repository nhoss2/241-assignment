#ifndef WIRE_H
#define WIRE_H

#include "part.h"

class Wire: public Part {

private:

    float *_wireLength;

    bool validateInputs(const float wireLength);

public:

    ~Wire(); //default deconstructor deletes all objects.

    Wire(const float wireEfficiency, const float wireLength);

    float getWireLength() const;

};

#endif
