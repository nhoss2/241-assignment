#ifndef INVERTER_H
#define INVERTER_H

#include "part.h"

class Inverter: public Part {

public:

    Inverter(const float inverterEfficiency);

    ~Inverter();

};

#endif
