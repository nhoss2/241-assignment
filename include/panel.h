//define include guards to protect against multiple inclusions and thus potential conflitcts
#ifndef PANEL_H
#define PANEL_H

#include <string>

class Panel {

private:
    // private member variables for panel class.

    float panelData[9];

    bool _valid;

    //checks if panel is valid if it isnt, we assume all data is garbage and set the _valid variable to 0
    bool is_valid_panelinput();

    float angle(const float angle);
    float panelOutput();

	float latitudeToAngle(const std::string latitude);



public:


    Panel();   // default constructor.

    ~Panel(); //default deconstructor deletes all objects.

    Panel(const float ratedWatts, const float degreesWest, const float orientationLoss, const float degreesHorizontal, const float angleLoss, const float reductionPerYear, const float effectiveSunshineHrs, std::string latitude);

	std::string optAngleRep(const float angle);

    // set the panel from datatypes
    void set_panel(const float ratedWatts=0, const float degreesWest=0, const float orientationLoss=0, const float degreesHorizontal=0, const float angleLoss=0, const float reductionPerYear=0, const float effectiveSunshineHrs=0, std::string latitude="0 0 0 N");


    // accessors and mutators

    float getRatedWatts() const {
        return panelData[0];
    }

    float getDegreesWest() const {
        return panelData[1];
    }

    float getOrientationLoss() const {
        return panelData[2];
    }

    float getDegreesHorizontal() const {
        return panelData[3];
    }

    float getAngleLoss() const {
        return panelData[4];
    }

    float getReductionPerYear() const {
        return panelData[5];
    }

    float getEffectiveSunshineHrs() const {
        return panelData[6];
    }

    float getOpAngle() const {
        return panelData[7];
    }
	
    float getOutputPower() const {
        return panelData[8];
    }

    bool getValid() const {
        return _valid;
    }


};

#endif




