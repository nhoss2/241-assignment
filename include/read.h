/* Read Class for solar panel program
 * ENB241
 * Tyler Morrison
 * n8100217
 */
 
#include <string>
	using std::string;
#include <sstream>
 #ifndef _READ_H
 #define _READ_H
 
 class Read {
	public:
	
		Read();
		Read(char *inputFile);
		
		~Read();
		string stringRepresentation() const;
		
    float getRatedWatts() {
        return ratedWatts;
    }

    float getDegreesWest(){
        return degreesWest;
    }

    float getOrientationLoss() {
        return orientationLoss;
    }

    float getDegreesHorizontal(){
        return degreesHorizontal;
    }
		
    float getAngleLoss(){
        return angleLoss;
    }
		
		float getWireLength(){
        return wireLength;
    }
		
		float getWireLoss(){
        return wireLoss;
    }
		
		float getInverterEfficiency(){
         return inverterEfficiency;
    }
		
		float getReductionPerYear(){
        return reductionPerYear;
    }
		
		float getEffectiveSunshineHrs(){
        return effectiveSunshineHrs;
    }

	private:
		
		float ratedWatts;
		
		float degreesWest;
		
		float orientationLoss;
		
		float degreesHorizontal;
		
		float angleLoss;
		
		float wireLength;
		
		float wireLoss;
		
		float inverterEfficiency;
		
		float reductionPerYear;
		
		float effectiveSunshineHrs;
	
 };

#endif

